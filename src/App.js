import './App.css';
import "bootstrap/dist/css/bootstrap.min.css"
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Home from './pages/Home';
import { Route, Routes, redirect } from 'react-router-dom';
import ProductList from './pages/ProductList';
import ProductInfo from './pages/ProductInfo';
import Cart from './pages/Cart';
import Login from './pages/Login';
import SignUp from './pages/SignUp';
import CheckOut from './pages/CheckOut';
import "primereact/resources/themes/lara-light-indigo/theme.css";
import "primereact/resources/primereact.css";

function App() {
  return (
    <>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/product" element={<ProductList />} exact />
        <Route path="/product/:productId" element={<ProductInfo />} />
        <Route path="/cart" element={<Cart />} />
        <Route path="/login" element={<Login />} />
        <Route path="/signup" element={<SignUp />} />
        <Route path="/checkout" element={<CheckOut />} />
        <Route path="*" element={<div>Not fount</div>} />
        
      </Routes>
    </>
  );
}

export default App;
