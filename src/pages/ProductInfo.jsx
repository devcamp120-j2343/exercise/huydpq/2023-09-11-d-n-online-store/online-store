import { KeyboardArrowRightOutlined } from "@mui/icons-material"
import Header from "../components/header"
import BreadCrumb from "../components/BreadCrumb"
import Footer from "../components/footer"
import ProductDetail from "../components/product-Detail"
import { useParams } from "react-router-dom"

const ProductInfo = () => {
    const {productId} = useParams()
    const dataBreadCrumb = [{
        name: "Home",
        url: "/",
        icon: <KeyboardArrowRightOutlined />
    },
    {
        name: "Product",
        url: "/product",
        icon: <KeyboardArrowRightOutlined />
    },
    {
        name: "ProductInFo",
        url: `/product/${productId}`
    }]

    return (
        <>
            <Header />
            <BreadCrumb data={dataBreadCrumb} />
            <ProductDetail />
            <Footer />
        </>
    )
}

export default ProductInfo