import { useEffect, useState } from "react"
import { useNavigate } from "react-router-dom"
import Header from "../components/header"
import BreadCrumb from "../components/BreadCrumb"
import Footer from "../components/footer"
import { KeyboardArrowRightOutlined } from "@mui/icons-material"
import LoginContent from "../components/loginContent"

const Login = () => {

    const dataBreadCrumb = [{
        name: "Home",
        url: "/",
        icon: <KeyboardArrowRightOutlined />
    },
    {
        name: "Login",
        url: "/login",

    }]
    const navigate = useNavigate()
    useEffect(() => {


    }, [])
    return (
        <>
            <Header />
            <BreadCrumb data={dataBreadCrumb} />
            <div className="min-h-[60vh]">
                <LoginContent />
            </div>
            <Footer />
        </>
    )
}

export default Login