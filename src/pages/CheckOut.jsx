import { useNavigate } from "react-router-dom"
import BreadCrumb from "../components/BreadCrumb"
import Footer from "../components/footer"
import Header from "../components/header"
import { useEffect } from "react"
import { KeyboardArrowRightOutlined } from "@mui/icons-material"
import CheckOutContent from "../components/checkOutContent"

const CheckOut = () => {
    const dataBreadCrumb = [{
        name: "Home",
        url: "/",
        icon: <KeyboardArrowRightOutlined />
    },
    {
        name: "CheckOut",
        url: "/checkout",

    }]
    const navigate = useNavigate()
    useEffect(() => {


    }, [])
    return (
        <>
            <Header />
            <BreadCrumb data={dataBreadCrumb} />
            <div className="min-h-[60vh]">
                <CheckOutContent />
            </div>
            <Footer />
        </>
    )
}

export default CheckOut