import { KeyboardArrowRightOutlined } from "@mui/icons-material"
import BreadCrumb from "../components/BreadCrumb"
import Footer from "../components/footer"
import Header from "../components/header"
import Product from "../components/product"

const ProductList = () => {
    const dataBreadCrumb = [{
        name: "Home",
        url: "/",
        icon: <KeyboardArrowRightOutlined />
    }, {
        name: "Product",
        url: "/product",
    }]

    return (
        <>
            <Header />
            <BreadCrumb data={dataBreadCrumb} />

            <Product />

            <Footer />
        </>
    )
}

export default ProductList