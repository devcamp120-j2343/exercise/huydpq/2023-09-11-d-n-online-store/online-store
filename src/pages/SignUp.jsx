import { useEffect, useState } from "react"
import { useNavigate } from "react-router-dom"
import Header from "../components/header"
import BreadCrumb from "../components/BreadCrumb"
import Footer from "../components/footer"
import { KeyboardArrowRightOutlined } from "@mui/icons-material"
import SignUpContent from "../components/signUpContent"

const SignUp = () => {

    const dataBreadCrumb = [{
        name: "Home",
        url: "/",
        icon: <KeyboardArrowRightOutlined />
    },
    {
        name: "SignUp",
        url: "/signup",

    }]
    const navigate = useNavigate()
    useEffect(() => {


    }, [])
    return (
        <>
            <Header />
            <BreadCrumb data={dataBreadCrumb} />
            <div className="min-h-[60vh]">
                <SignUpContent />
            </div>
            <Footer />
        </>
    )
}

export default SignUp