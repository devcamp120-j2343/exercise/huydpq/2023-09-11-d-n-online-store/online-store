import { KeyboardArrowRightOutlined } from "@mui/icons-material"
import CartContent from "../components/cart"
import Footer from "../components/footer"
import Header from "../components/header"
import BreadCrumb from "../components/BreadCrumb"

const Cart = () => {
    const dataBreadCrumb = [{
        name: "Home",
        url: "/",
        icon: <KeyboardArrowRightOutlined />
     }, {
        name: "Cart",
        url: "/cart",
    }]
    return (
        <>
            <Header />
            <BreadCrumb data={dataBreadCrumb} />
            <div className="min-h-[60vh]">
                <CartContent />
            </div>
            <Footer />
        </>
    )
}

export default Cart