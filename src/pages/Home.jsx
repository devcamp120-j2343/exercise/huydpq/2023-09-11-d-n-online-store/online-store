import Footer from "../components/footer"
import Header from "../components/header"
import HomePageContent from "../components/homePageContent"

const Home = () => {
    return(
        <>
        <Header />
        <HomePageContent />
        <Footer />
        </>
    )
}

export default Home