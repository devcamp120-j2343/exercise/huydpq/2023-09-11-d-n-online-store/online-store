import { API_GET_USER_INFO_SUCCESS, AXIOS_LOGIN_SUCCESS, CALL_API_COUNTRY, CALL_API_CREATE_ORDER_SUCCESS, ON_CHANGE_INPUT_FROM, ON_CHANGE_SELECT_CITY, ON_CHANGE_SELECT_COUNTRY } from "../constants/product.constant";

const initialState = {
    selectCountry: [],
    selectCity: [],
    customer: {
        fullName: "",
        phone: "",
        email: "",
        address: "",
        city: "0",
        country: "0",
    }


}
export const CheckoutReducer = (state = initialState, action) => {
    switch (action.type) {
        case CALL_API_CREATE_ORDER_SUCCESS:
            localStorage.setItem("newOrder", JSON.stringify(action.data))
            break;
        case CALL_API_COUNTRY:
            state.selectCountry = action.data
            state.selectCountry.map((item) => {
                if (item.country === state.customer.country) {
                    state.selectCity = item.cities
                    return
                }
            })
            break;
        case API_GET_USER_INFO_SUCCESS:
           
            for (let key in state.customer) {
                state.customer[key] = action.data[key]
            }
            break;
        case ON_CHANGE_INPUT_FROM:
            state.customer[action.name] = action.value
            break;
        case ON_CHANGE_SELECT_COUNTRY:
            state.customer.city = "0"
            state.selectCity = []
            state.selectCountry.map((item) => {
                if (item.country === action.value) {
                    state.selectCity = item.cities
                    return
                }
            })
            state.customer.country = action.value
            break;
        case ON_CHANGE_SELECT_CITY:
            state.customer.city = action.value
            break;
        case AXIOS_LOGIN_SUCCESS:
            localStorage.setItem("token", JSON.stringify(action.data))
            break;

        default:
            break;
    }
    return { ...state }
}


