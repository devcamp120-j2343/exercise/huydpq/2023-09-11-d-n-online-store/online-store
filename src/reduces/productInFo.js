import {AXIOS_PRODUCT_BY_ID_PENDING, AXIOS_PRODUCT_BY_ID_SUCCESS } from "../constants/product.constant";

const initialState = {
    pending: false,
    producInFo: [],
    relatedProducts: [],

}
export const ProductInFoReducer = (state = initialState, action) => {
    switch (action.type) {
        case AXIOS_PRODUCT_BY_ID_PENDING:
            state.pending = true
            break;
        case AXIOS_PRODUCT_BY_ID_SUCCESS:
            state.pending = false
            state.producInFo = action.data
            state.relatedProducts = action.dataType
            break;
        default:
            break;
    }
    return { ...state }
}