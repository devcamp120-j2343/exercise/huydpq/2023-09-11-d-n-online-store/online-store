import {GET_PRODUCT_QUATITY } from "../constants/product.constant";

const initialState = {
    listOrder: [],
    total: 0,
}
export const CartReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_PRODUCT_QUATITY:
           if(action.payload){
            state.listOrder = action.payload
            state.total = 0
            for(let element of state.listOrder){
                state.total += (element.promotionPrice * element.quantity)
            }
           }
            break;
        default:
            break;
    }
    return { ...state }
}


