import { AXIOS_PENDING_PRODUCT_LIST, AXIOS_SUCCESS_PRODUCT_LIST, ONCHANGE_CHECK_BOX, ON_CHANGE_INPUT_FILTER, ON_CHANGE_PAGINATION, ON_CLICK_CLEAR, ON_CLICK_FILTER } from "../constants/product.constant"

const initialState = {
    pending: false,
    productList: [],
    brank: "",
    min: "",
    max: "",
    limit: 8,
    noPage: 0,
    currentPage: 1,

}
export const FilterReducer = (state = initialState, action) => {
    switch (action.type) {
        case AXIOS_PENDING_PRODUCT_LIST:
            state.pending = true
            break;
        case AXIOS_SUCCESS_PRODUCT_LIST:
            state.pending = false
            state.noPage = Math.ceil(action.dataTotal / state.limit)
            state.productList = action.data
            break;
        case ONCHANGE_CHECK_BOX:
            let name = action.name
            state[name] = action.value
            break;
        case ON_CHANGE_INPUT_FILTER:
            let inputName = action.name
            state[inputName] = action.value
            break;
        case ON_CHANGE_PAGINATION:
            state.currentPage = action.payload
            break;
        case ON_CLICK_FILTER:
            state.currentPage = action.payload
            break;
        case ON_CLICK_CLEAR:
            state.brank = ""
            state.min = ""
            state.max = ""
            break;
        default:
            break;
    }
    return { ...state }
}