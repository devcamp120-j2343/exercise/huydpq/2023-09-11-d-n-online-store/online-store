import { AXIOS_PRODUCT_ERROR, AXIOS_PRODUCT_SUCCESS, GET_PRODUCT_QUATITY } from "../constants/product.constant";

const initialState = {
    allProduct: [],
    quantityProduct: 0
}
export const ProductReducer = (state = initialState, action) => {
    switch (action.type) {
        case AXIOS_PRODUCT_SUCCESS:
            state.allProduct = action.data
            break;
        case AXIOS_PRODUCT_ERROR:
            break;
        case GET_PRODUCT_QUATITY:
            if(action.payload){
                state.quantityProduct = action.payload.length
            } else {
                state.quantityProduct = 0
            }
            
        break;
        default:
            break;
    }
    return { ...state }
}

