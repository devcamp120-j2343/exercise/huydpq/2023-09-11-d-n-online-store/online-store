import { combineReducers } from "redux";
import { ProductReducer } from "./product";
import { FilterReducer } from "./filterProduct.js";
import { ProductInFoReducer } from "./productInFo.js";
import { CartReducer } from "./cartReduct";
import {CheckoutReducer} from "./chechOutReducer"
const rootReducer = combineReducers({
    ProductReducer,
    FilterReducer,
    ProductInFoReducer,
    CartReducer,
    CheckoutReducer
})

export default rootReducer