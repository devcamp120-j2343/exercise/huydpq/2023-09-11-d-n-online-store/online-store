export const AXIOS_PRODUCT_SUCCESS = "Trang thái thành công khi gọi API All product trang home"

export const AXIOS_PRODUCT_ERROR = "Trang thái lỗi khi gọi API All product  trang home"

export const AXIOS_PENDING_PRODUCT_LIST = "Trang thái chờ khi gọi API product All product trang productList"

export const AXIOS_SUCCESS_PRODUCT_LIST = "Trang thái thành công khi gọi API All product trang productList"

export const AXIOS_ERROR_PRODUCT_LIST = "Trang thái lỗi khi gọi API All trang productList"

export const AXIOS_PRODUCT_BY_ID_PENDING = "Trang thái chờ khi gọi API product by id"

export const AXIOS_PRODUCT_BY_ID_SUCCESS = "Trang thái khi gọi API product by id thành công"

export const AXIOS_PRODUCT_BY_ID_ERROR = "Trang thái lỗi khi gọi API product by id thất bại "

export const AXIOS_LOGIN_SUCCESS = "Trang thái thành công khi gọi API log in"

export const AXIOS_LOGIN_ERROR = "Trang thái lỗi khi gọi API log in thất bại "

export const AXIOS_SIGNUP_SUCCESS = "Trang thái thành công khi gọi API SIGNUP"

export const AXIOS_SIGNUP_ERROR = "Trang thái lỗi khi gọi API SIGNUP thất bại "

export const ONCHANGE_CHECK_BOX = "Sự kiện onChange check box"

export const ON_CHANGE_PAGINATION = "Sự kiện onChange phan trang"

export const ON_CLICK_FILTER = "Sự kiện lọc"

export const ON_CLICK_CLEAR = "Sự kiện chuyển trnag"

export const ON_CLICK_PRODYCT_INFO = "Sự kiện click chuyển trang product info"

export const ADD_TO_CARD = "Sự kiện thêm vào giỏ hàng"

export const GET_PRODUCT_QUATITY = "Sự kiện lấy số lượng sản phẩm trong giở hàng"

export const ON_CLICK_UP_QUANTITY = "Sự kiện tăng số lượng sản phẩm"

export const ON_CLICK_DOWN_QUANTITY = "Sự kiện giảm số lượng sản phẩm"

export const GET_LIST_ORDER_TO_LOCAL = "Sự kiện lấy dữ liệu từ local"

export const CALL_API_CREATE_ORDER_SUCCESS = "Call api tạo order thành công"

export const CALL_API_CREATE_ORDER_ERROR = "Call api tạo order thất bại "

export const API_GET_USER_INFO_SUCCESS = "Call api thành công lấy thông tin mua hàng gần nhất của User "

export const API_GET_USER_INFO_ERROR = "Call api thất bại lấy thông tin mua hàng gần nhất của User "

export const ON_CHANGE_INPUT_FILTER = "Sự kiện onChange input FIlter"

export const CALL_API_COUNTRY = "Call api quốc gia"

export const ON_CHANGE_SELECT_COUNTRY = "Sự kiện change select country";

export const ON_CHANGE_SELECT_CITY = "Sự kiện change select city";

export const ON_CHANGE_INPUT_FROM = "Sự kiện onChange input form"



