import axios from "axios"
import { AXIOS_PRODUCT_BY_ID_ERROR, AXIOS_PRODUCT_BY_ID_PENDING, AXIOS_PRODUCT_BY_ID_SUCCESS, ONCHANGE_CHECK_BOX, ON_CHANGE_PAGINATION, ON_CLICK_FILTER, ON_CLICK_CLEAR, AXIOS_SUCCESS_PRODUCT_LIST, AXIOS_ERROR_PRODUCT_LIST, AXIOS_PRODUCT_SUCCESS, AXIOS_PRODUCT_ERROR, AXIOS_PENDING_PRODUCT_LIST, ADD_TO_CARD, GET_PRODUCT_QUATITY, CALL_API_CREATE_ORDER_ERROR, CALL_API_CREATE_ORDER_SUCCESS, CALL_API_COUNTRY, ON_CHANGE_SELECT_COUNTRY, ON_CHANGE_SELECT_CITY, AXIOS_LOGIN_SUCCESS, AXIOS_LOGIN_ERROR, AXIOS_SIGNUP_SUCCESS, AXIOS_SIGNUP_ERROR, ON_CHANGE_INPUT_FROM, ON_CHANGE_INPUT_FILTER, API_GET_USER_INFO_ERROR, API_GET_USER_INFO_SUCCESS } from "../constants/product.constant";

export const getProductList = (page, limit, brank, min, max) => {
    return async (dispatch) => {
        try {
            await dispatch({
                type: AXIOS_PENDING_PRODUCT_LIST,
            })
            const param = new URLSearchParams({
                "_page": (page - 1) * limit,
                "_limit": limit,
                "brank": brank,
                "min": min,
                "max": max
            })
            let config = {
                method: 'get',
                url: 'http://localhost:8000/product?' + param.toString(),
                headers: {
                    'Content-Type': 'application/json'
                }
            };
            const reponse = await axios(config)
            const listData = reponse.data

            return dispatch({
                type: AXIOS_SUCCESS_PRODUCT_LIST,
                dataTotal: listData.total,
                data: listData.data
            })
        } catch (error) {

            return dispatch({
                type: AXIOS_ERROR_PRODUCT_LIST,
                err: error
            })
        }
    }
}
export const getProduct = () => {
    return async (dispatch) => {
        try {
            let config = {
                method: 'get',
                url: 'http://localhost:8000/product',
                headers: {
                    'Content-Type': 'application/json'
                }
            };
            const reponse = await axios(config)
            const listData = reponse.data.data
            return dispatch({
                type: AXIOS_PRODUCT_SUCCESS,
                data: listData
            })
        } catch (error) {

            return dispatch({
                type: AXIOS_PRODUCT_ERROR,
                err: error
            })
        }
    }
}

// Call api get product by id lấy chi tiết sản phẩm
export const getProductById = (id) => {
    return async (dispatch) => {
        try {

            await dispatch({
                type: AXIOS_PRODUCT_BY_ID_PENDING,
            })
            const reponse = await axios({
                method: 'get',
                url: 'http://localhost:8000/product/' + id,
                headers: {
                    'Content-Type': 'application/json'
                }
            });
            const productInFo = await reponse.data

            return dispatch({
                type: AXIOS_PRODUCT_BY_ID_SUCCESS,
                dataType: productInFo.dataType,
                data: productInFo.data
            })
        } catch (error) {
            return dispatch({
                type: AXIOS_PRODUCT_BY_ID_ERROR,
                err: error
            })
        }
    }
}
// Call api tạo đơn hàng 
export const createOrder = (listOrder, total) => {
    const orderDetail = listOrder.map((item) => {
        return { product: item._id, quantity: item.quantity }
    })
    return async (dispatch) => {
        try {
            const reponse = await axios({
                method: 'post',
                url: 'http://localhost:8000/order',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: JSON.stringify({
                    orderDetails: orderDetail,
                    cost: total
                })
            });
            const dataOrder = await reponse.data

            return dispatch({
                type: CALL_API_CREATE_ORDER_SUCCESS,
                data: dataOrder.data
            })

        } catch (error) {
            return dispatch({
                type: CALL_API_CREATE_ORDER_ERROR,
                err: error
            })
        }
    }
}
// Call api get user info

export const getUserInfo = (token) => {
    return async (dispatch) => {
        try {
            
            const reponse = await axios({
                method: 'get',
                maxBodyLength: Infinity,
                url: 'http://localhost:8000/userInfo/' + token.userId,
                headers: {
                    'Content-Type': 'application/json'
                }
            });
            const userInfo = await reponse.data
            
            return dispatch({
                type: API_GET_USER_INFO_SUCCESS,
                data: userInfo.data
            })
        } catch (error) {
            return dispatch({
                type: API_GET_USER_INFO_ERROR,
                err: error
            })
        }
    }
}
// Call api lấy danh sách các quốc gia
export const callapiCountry = () => {
    return async (dispatch) => {
        try {

            const reponse = await axios({
                method: 'get',
                maxBodyLength: Infinity,
                url: 'https://countriesnow.space/api/v0.1/countries',
                headers: {}
            })
            const dataCountry = await reponse.data
            return dispatch({
                type: CALL_API_COUNTRY,
                data: dataCountry.data
            })
        } catch (error) {
            return
        }
    }
}
// api login
export const CallApiLogin = (data, toasts, setToken, reset) => {
    return async (dispatch) => {
        try {

            const reponse = await axios({
                method: 'post',
                maxBodyLength: Infinity,
                url: 'http://localhost:8000/auth/login',
                headers: {},
                data: data
            })
            const dataLogin = await reponse.data
            await toasts("success", " Login Successfully")
            await setToken(true)
            await reset()
            return dispatch({
                type: AXIOS_LOGIN_SUCCESS,
                data: dataLogin
            })
        } catch (error) {
            toasts("warn", error.response.data.message)
            return dispatch({
                type: AXIOS_LOGIN_ERROR,
                err: error
            })

        }
    }
}

// api SignUp
export const CallApiSignUp = (data, toasts, setToken, reset) => {
    return async (dispatch) => {
        try {

            const reponse = await axios({
                method: 'post',
                maxBodyLength: Infinity,
                url: 'http://localhost:8000/auth/signup',
                headers: {},
                data: data
            })
            const dataSignUp = await reponse.data
            await toasts("success", " Singup Successfully")
            await setToken(true)
            await reset()
            return dispatch({
                type: AXIOS_SIGNUP_SUCCESS,
                data: dataSignUp
            })
        } catch (error) {
            toasts("warn", error.response.data.message)
            return dispatch({
                type: AXIOS_SIGNUP_ERROR,
                err: error
            })

        }
    }
}
// hàm change select Country
export const changeSelectCountry = (value) => {
    return ({
        type: ON_CHANGE_SELECT_COUNTRY,
        value: value
    })
}

// hàm change select City
export const changeSelectCity = (value) => {
    return ({
        type: ON_CHANGE_SELECT_CITY,
        value: value
    })
}

export const changeInputForm = (value, filename) => {
    return ({
        type: ON_CHANGE_INPUT_FROM,
        name: filename,
        value: value
    })
}

export const clearData = () => {
    return ({
        type: ON_CLICK_CLEAR,
        payload: []
    })
}
// phân trang
export const pageChangePagination = (value) => {
    return ({
        type: ON_CHANGE_PAGINATION,
        payload: value
    })
}
// hàm onChange change CheckBox filter
export const onChangeCheckBox = (value, filname) => {
    return ({
        type: ONCHANGE_CHECK_BOX,
        value,
        name: filname
    })
}

// hàm onChange change input filter
export const changeInputFilter = (value, filname) => {
    return ({
        type: ON_CHANGE_INPUT_FILTER,
        value,
        name: filname
    })
}
// hàm click filter
export const onClickFilter = () => {
    return ({
        type: ON_CLICK_FILTER,
        payload: 1,

    })
}
// thêm vào giò hàng
export const addToCard = (quatity) => {
    return ({
        type: ADD_TO_CARD,
        payload: quatity
    })
}

export const getProductQuatity = () => {
    const dataLocal = localStorage.getItem("listProduct")
    return ({
        type: GET_PRODUCT_QUATITY,
        payload: JSON.parse(dataLocal)
    })
}
