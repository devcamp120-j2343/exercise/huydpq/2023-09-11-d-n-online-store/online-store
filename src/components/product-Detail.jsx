import { useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { useParams } from "react-router-dom"
import { getProductById, getProductQuatity } from "../actions/product.action"
import CardPoduct from "./cardProducts"
import { CircularProgress } from "@mui/material"

const ProductDetail = () => {
    const dispatch = useDispatch()
    const [quantity, setQuatity] = useState(1)
    let { productId } = useParams()
    const { producInFo, relatedProducts, pending } = useSelector((redux) => redux.ProductInFoReducer)

    useEffect(() => {
        dispatch(getProductById(productId))

    }, [])

    const dataLoacal = { ...producInFo, quantity }

    const onCickAđToCard = () => {
        let productList = localStorage.getItem("listProduct")
        let arr = JSON.parse(productList)
        if (productList) {
            const check = arr.some((item, index) => {
                if (item._id === dataLoacal._id) {
                    item.quantity = item.quantity + 1
                    arr.fill(item, index, index + 1)
                }
                return item._id === dataLoacal._id
            })

            if (check) {
                localStorage.setItem("listProduct", JSON.stringify(arr))
            } else {
                arr.push(dataLoacal)
                localStorage.setItem("listProduct", JSON.stringify(arr))
            }

        } else {
            localStorage.setItem("listProduct", JSON.stringify([dataLoacal]))
        }
        dispatch(getProductQuatity())
    }
    return (
        <>
            {
                pending ?
                    <div className="container my-7 flex justify-center">
                        <CircularProgress />
                    </div>
                    : <div className="container my-7 mx-auto">
                        <div className="grid grid-cols-3 ">
                            <div className="col-span-2">
                                <img src={producInFo.imageUrl} alt="abc" width="65%" />
                            </div>
                            <div className="flex flex-col gap-3">
                                <p className="text-4xl text-[#1d1d1f] font-Inter">{producInFo.name}</p>
                                <p className="text-[#637381] text-base mb-12 font-normal not-italic">{producInFo.description}</p>
                                <p className=" text-[#637381] text-2xl font-Inter line-through font-normal not-italic">Price: $ {producInFo.buyPrice}</p>
                                <p className="text-[#1d1d1f] text-3xl font-Inter">Promotion Price: $ {producInFo.promotionPrice}</p>
                                <p className="text-[#637381] text-lg">Quantity: {producInFo.amount}</p>
                                <button onClick={onCickAđToCard} className=" bg-black font-medium text-xl font-Inter text-white p-3 w-40 rounded ">Add to cart</button>
                            </div>

                        </div>
                        <div className="mt-[5rem] mb-8">
                            <p className="text-[#1d1d1f] text-2xl font-Inter">Description</p>
                        </div>
                        <div className="flex flex-col gap-3">
                            <p className="text-[#637381] text-base mb-12 font-normal not-italic">{producInFo.description}</p>
                            <div className="mx-auto">
                                <img src={producInFo.imageUrl} alt="abc" width="90%" />

                            </div>
                            <div className="my-8">
                                <p className="text-[#1d1d1f] text-2xl font-Inter">Related Products</p>
                            </div>

                            <div className="grid grid-cols-4 gap-[30px]">
                                {
                                    relatedProducts && relatedProducts.length > 0 && relatedProducts.map((item, index) => {
                                        return <CardPoduct key={index} data={item} />
                                    })
                                }
                            </div>
                        </div>
                    </div>
            }
        </>
    )
}

export default ProductDetail