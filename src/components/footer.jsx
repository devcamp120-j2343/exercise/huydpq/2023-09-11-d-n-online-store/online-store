import { EmailOutlined, Facebook, Instagram, Twitter } from "@mui/icons-material"

const Footer = () => {
    return (
        <div className="bg-[#f0f0f7] container-fluid">
            <section className="grid grid-cols-4 gap-4 mx-44 py-5">
                <section className="grid grid-rows-4">
                    <h5>About</h5>
                    <p><a href="#/" className=" text-[#0000008a]">About Us</a></p>
                    <p><a href="#/" className=" text-[#0000008a]">Team</a></p>
                    <p><a href="#/" className=" text-[#0000008a]">Blog</a></p>
                </section>
                <section className="grid grid-rows-4">
                    <h5>Services</h5>
                    <p><a href="#/" className=" text-[#0000008a]">About Us</a></p>
                    <p><a href="#/" className=" text-[#0000008a]">Team</a></p>
                    <p><a href="#/" className=" text-[#0000008a]">Blog</a></p>
                </section>
                <section className="grid grid-rows-4">
                    <h5>About</h5>
                    <p><a href="#/" className=" text-[#0000008a]">About Us</a></p>
                    <p><a href="#/" className=" text-[#0000008a]">Team</a></p>
                    <p><a href="#/" className=" text-[#0000008a]">Blog</a></p>
                </section>
                <section className="text-center">
                    <p className="font-bold text-3xl">DEVCAMP</p>
                    <section>
                        <a href="#/" className="mx-2 text-black"><Facebook /></a>
                        <a href="#/" className="mx-2 text-black"><EmailOutlined /></a>
                        <a href="#/" className="mx-2 text-black"><Instagram /></a>
                        <a href="#/" className="mx-2 text-black"><Twitter /></a>
                    </section>
                </section>
            </section>
        </div>
    )
}

export default Footer