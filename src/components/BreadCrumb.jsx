import { Link } from "react-router-dom"

const BreadCrumb = ({ data}) => {
    return (
        <div className="flex font-Inter text-base py-3 container">
            {
                data.map((item, index) => {
                    return <div key={index}>
                        <a href={item.url} className="text-[#1d1d1f] no-underline mr-2">{item.name} </a>
                        <span className="mr-2">{item.icon}</span>
                    </div>
                })
            }
        </div>

       
    )
}

export default BreadCrumb