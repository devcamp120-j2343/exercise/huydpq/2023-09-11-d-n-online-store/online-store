import { useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import { createOrder, getProductQuatity } from "../actions/product.action"
import { Button, ButtonGroup } from "reactstrap"
import AddIcon from '@mui/icons-material/Add';
import RemoveIcon from '@mui/icons-material/Remove';
import { Link, useNavigate } from "react-router-dom";
const CartContent = () => {
    const dispatch = useDispatch()
    const navigate = useNavigate()
    const { listOrder, total } = useSelector((redux) => redux.CartReducer)
    const productList = localStorage.getItem("listProduct")
    const arr = JSON.parse(productList)
    useEffect(() => {
        dispatch(getProductQuatity())
    }, [])

    const onBtnUpClick = (product) => {
        arr.some((item, index) => {
            if (item._id === product._id) {
                item.quantity = item.quantity + 1
                arr.fill(item, index, index + 1)
                localStorage.setItem("listProduct", JSON.stringify(arr))
            }

        })
        dispatch(getProductQuatity())
    }

    const onBtnDownClick = (product) => {
        arr.some((item, index) => {
            if (item._id === product._id) {
                if (item.quantity === 1) {
                    arr.splice(index, 1)
                    localStorage.setItem("listProduct", JSON.stringify(arr))
                } else {
                    item.quantity = item.quantity - 1
                    arr.fill(item, index, index + 1)
                    localStorage.setItem("listProduct", JSON.stringify(arr))
                }

            }

        })
        dispatch(getProductQuatity())
    }
    const onBtnCreateOrder = () => {
        dispatch(createOrder(arr, total))
        let token = localStorage.getItem("token")
        if (token) {
            navigate("/checkout")
        } else {
            navigate("/login")
        }


    }

    return (
        <div className="container my-7 ">
            <div className="grid grid-cols-2 mx-auto">
                <div>
                    <p className="text-[#1d1d1f] font-Inter text-lg">Pruducts</p>
                </div>
                <div className="flex justify-around">
                    <div><p className="text-[#1d1d1f] font-Inter text-lg p-1">Price</p></div>
                    <div><p className="text-[#1d1d1f] font-Inter text-lg p-1 ">Quantity</p></div>
                    <div><p className="text-[#1d1d1f] font-Inter text-lg p-1">Total</p></div>

                </div>
            </div>
            <hr />
            <div className="mt-7 mx-auto">
                {
                    listOrder.map((items, index) => {
                        return (

                            <div key={index} className="grid grid-cols-2 mt-8 pb-4 border-b border-[#c7c8c9] ">
                                <div className="flex gap-8 items-center">
                                    <img src={items.imageUrl} alt="abc" width="30%" />
                                    <p className="text-[#1d1d1f] font-Inter text-lg p-1">${items.name}</p>
                                </div>
                                <div className="flex justify-around items-center">
                                    <div>
                                        <p className="text-[#1d1d1f] font-Inter text-lg p-3">${items.promotionPrice}</p>
                                    </div>
                                    <div className="">
                                        <ButtonGroup className="btn-group" role="group" aria-label="Basic example">
                                            <Button onClick={() => onBtnUpClick(items)} className="btn btn-secondary">
                                                <AddIcon />
                                            </Button>
                                            <Button className="bg-[#ad6464] p-3">{items.quantity}</Button>
                                            <Button onClick={() => onBtnDownClick(items)} className="btn btn-secondary">
                                                <RemoveIcon />
                                            </Button>
                                        </ButtonGroup></div>
                                    <div><p className="text-[#1d1d1f] font-Inter text-lg p-3">${items.promotionPrice * items.quantity}</p></div>

                                </div>
                            </div>
                        )
                    })
                }

            </div>
            <div className="mt-[3rem]">
                <div className="w-[50%] ml-auto bg-[#f0f0f7] p-4">
                    <div className="text-[#1d1d1f] font-Inter text-xl">
                        Cart Total
                    </div>
                    <hr />
                    <div className="flex justify-between text-[#1d1d1f] font-Inter text-lg mt-4">
                        <p>Total</p>
                        <p>$ {total}</p>
                    </div>
                    <div className="mt-2">
                        <Button onClick={onBtnCreateOrder} block size="lg"><Link >PROCEED TO CHECKOUT</Link></Button>
                    </div>
                </div>
            </div>


        </div>

    )
}
export default CartContent