import Slider from "react-slick"
import slider1 from "../assets/images/slider1.jpg"
import slider2 from "../assets/images/slider2.jpg"
import slider3 from "../assets/images/slider3.jpg"
import { useDispatch, useSelector } from "react-redux"
import { useEffect } from "react"
import { getProduct } from "../actions/product.action"
import CardPoduct from "./cardProducts"
const HomePageContent = () => {
    const dispatch = useDispatch()
    const { allProduct } = useSelector((reduxData) => reduxData.ProductReducer)
    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,

    };
    useEffect(() => {
        dispatch(getProduct(0,0))
    }, [])

    return (
        <section className="mx-auto min-h-[50vh] container mb-6">
            <div className="slider-img mx-auto px-14">
                <Slider {...settings}>
                    <div>
                        <img src={slider1} alt="slider-1" width="100%" />
                    </div>
                    <div>
                        <img src={slider2} alt="slider-2" width="100%" />
                    </div>
                    <div>
                        <img src={slider3} alt="slider-3" width="100%" />
                    </div>
                </Slider>
            </div>

            <div className="px-10">
                <div className="text-center">
                    <p className="font-Inter mt-5 text-3xl font-bold">Latest Products</p>
                </div>
                <div className="grid grid-cols-4 max-w-[1440px] mx-auto gap-[30px]">
                    {
                        allProduct.slice(0, 8).map((item, index) => {
                            return <CardPoduct key={index} data={item} />
                        })
                    }
                </div>
                <div className="text-center">
                    <a href="/product">
                        <button className="bg-black font-medium text-white p-2 w-44 rounded ">View All</button>
                    </a>
                </div>
            </div>

        </section>

    )
}

export default HomePageContent