import { Nav, NavItem, Navbar, NavbarBrand } from "reactstrap"
import NotificationsNoneIcon from '@mui/icons-material/NotificationsNone';
import AccountCircleOutlinedIcon from '@mui/icons-material/AccountCircleOutlined';
import ShoppingCartOutlinedIcon from '@mui/icons-material/ShoppingCartOutlined';
import logo from "../assets/images/Phone store logo - Made with PosterMyWall (1).jpg"
import { Link, NavLink, useNavigate } from "react-router-dom"
import { IconButton } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { clearData, getProductQuatity } from "../actions/product.action";
import { useEffect,useState  } from "react";
import Login from "../pages/Login";
// import { useState } from "react";
const Header = () => {
    const dispatch = useDispatch()
    const {quantityProduct} = useSelector(redux => redux.ProductReducer)
    
   const clear = () => {
    dispatch(clearData())
   }

   useEffect ( () => {
    dispatch(getProductQuatity())
   }, [])

    return (
        <header className="mb-2">
            <Navbar className="items-center" container >
                <NavbarBrand href="/"><img src={logo} alt="abc" style={{ height: 40, width: 250 }} /></NavbarBrand>
                <Nav className="me-auto">
                    <NavItem id="sidebar">
                        <NavLink
                            to="/"
                            className="nav-link link"
                            onClick={clear}
                        >Home</NavLink>
                    </NavItem>
                    <NavItem id="sidebar">
                        <NavLink
                            to="/product"
                            className="nav-link link"
                           
                        >Product</NavLink>
                    </NavItem>
                </Nav>
                <div className="ms-auto flex items-center">

                    <Link className="nav-link" to="/sss">
                        <IconButton>
                            <NotificationsNoneIcon sx={{ color: "black" }} />
                        </IconButton>
                    </Link>
                    <Link className="nav-link" to="/login" >
                        <IconButton >
                            <AccountCircleOutlinedIcon  sx={{ color: "black" }} />
                        </IconButton>
                    </Link>
                    <Link className="nav-link" to="/cart">
                        <IconButton>
                            <ShoppingCartOutlinedIcon sx={{ color: "black" }}></ShoppingCartOutlinedIcon>
                        </IconButton>
                    </Link>
                    <div>
                        <span className="bg-black text-white flex relative w-5 h-5 right-[18px] top-[-13px] justify-center items-center rounded-full">
                            {quantityProduct}
                        </span>
                    </div>
                </div>
            </Navbar>
            
        </header>
    )
}

export default Header