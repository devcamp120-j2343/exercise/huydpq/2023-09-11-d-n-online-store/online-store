import { Person2Outlined, VisibilityOffOutlined, VisibilityOutlined } from "@mui/icons-material"
import { Box, Button, FormControl, FormHelperText, Grid, IconButton, InputAdornment, InputLabel, OutlinedInput, Typography } from "@mui/material"
import { useEffect, useRef, useState } from "react";
import { useForm } from "react-hook-form";
import { useDispatch } from "react-redux";
import { CallApiSignUp } from "../actions/product.action";
import { Toast } from "primereact/toast";
import { Link, useNavigate } from "react-router-dom";
import LogOut from "./logOut";
const SignUpContent = () => {
    const [token, setToken] = useState(false)
    const [login, setlogin] = useState(false)
    const [showPassword, setShowPassword] = useState(false);
    const toast = useRef(null);
    const dispatch = useDispatch()
    const navigate = useNavigate()

    const { register, handleSubmit, reset, formState: { errors } } = useForm()

    const handleClickShowPassword = () => setShowPassword((show) => !show);

    const getToken = localStorage.getItem("token")
    useEffect(() => {

        if (getToken) {
            setlogin(true)
            setToken(true)
        }
    }, [token])

    const handleSubmitSignUp = (data) => {
        const toasts = (severity, mes) => toast.current.show({ severity: severity, detail: mes, life: 2000 });
        dispatch(CallApiSignUp(data, toasts, setToken, reset));

    }
    return (
        <div className="container mt-[1rem] mb-[3rem]">
            <Toast ref={toast} />
            {
                token ?
                    <div>
                        {
                            login ?
                                <LogOut setToken={() => setToken(false)} />
                                : <Grid container sx={{ mb: "30px", alignItems: "center", height: "300px" }}>
                                    <Grid item sm={12} md={12} lg={12} xl={5} xxl={5} sx={{ borderRadius: '5px', mx: "auto" }}>
                                        <Typography sx={{ fontFamily: "Inter" }} align="center" variant="h4">Login to coutinue</Typography>
                                        <Button
                                            sx={{
                                                backgroundColor: "#000000",
                                                ":hover": { backgroundColor: "#000000", },
                                                mt: 3
                                            }}
                                            fullWidth
                                            variant="contained"
                                            size="large"
                                            onClick={() => { navigate("/login") }}>
                                            Login
                                        </Button>
                                    </Grid>
                                </Grid>
                        }
                    </div>
                    : <Grid container sx={{ mb: "30px" }}>
                        <Grid item sm={12} md={12} lg={12} xl={5} xxl={5} sx={{ borderRadius: '5px', mx: "auto", border: "solid 1.5px" }} >
                            <Box sx={{ p: 5 }} component="form" onSubmit={handleSubmit(handleSubmitSignUp)}>
                                <Typography variant="h3" className="">SignUp</Typography>
                                <Typography sx={{ mt: 1 }} variant="h5" className="">to coutinue</Typography>

                                <FormControl error={errors.username ? true : false} sx={{ mt: '60px', width: '100%' }} variant="outlined">
                                    <InputLabel >Username</InputLabel>
                                    <OutlinedInput
                                        sx={{ borderRadius: "8px" }}
                                        label="Username"
                                        endAdornment={
                                            <InputAdornment position="end">
                                                <IconButton>
                                                    <Person2Outlined />
                                                </IconButton>
                                            </InputAdornment>
                                        }
                                        {...register("username", { required: "Username is required." })}
                                    />
                                    <FormHelperText>{errors.username?.message}</FormHelperText>
                                </FormControl>
                                <FormControl error={errors.password ? true : false} sx={{ mt: '40px', width: '100%' }} variant="outlined">
                                    <InputLabel htmlFor="password" >Password</InputLabel>
                                    <OutlinedInput
                                        label="password"
                                        sx={{ borderRadius: "8px" }}
                                        type={showPassword ? 'text' : 'password'}
                                        endAdornment={
                                            <InputAdornment position="end">
                                                <IconButton
                                                    aria-label="toggle password visibility"
                                                    onClick={handleClickShowPassword}
                                                >
                                                    {showPassword ? <VisibilityOffOutlined /> : <VisibilityOutlined />}
                                                </IconButton>
                                            </InputAdornment>
                                        }
                                        {...register("password", { required: "Password is required." })}
                                    />
                                    <FormHelperText>{errors.password?.message}</FormHelperText>
                                </FormControl>
                                <FormControl sx={{ mt: 7, mb: 10, width: "100%" }}>
                                    <Button
                                        type="submit"
                                        variant="contained"
                                        size="large"
                                        sx={{
                                            backgroundColor: "#000000",
                                            ":hover": { backgroundColor: "#000000", }
                                        }}
                                    >
                                        SignUp
                                    </Button>
                                </FormControl>

                            </Box>
                        </Grid>
                    </Grid>

            }
        </div>
    )
}

export default SignUpContent