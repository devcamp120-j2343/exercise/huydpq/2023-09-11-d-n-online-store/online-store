import { Button, Grid, Typography } from "@mui/material"
import { useNavigate } from "react-router-dom"

const LogOut = ({ setToken }) => {
    const navigate = useNavigate()

    const onBtnLogOut = () => {
        
        setToken()
        localStorage.removeItem("token")
        navigate("/login")
    }
    const payment = () => {
        navigate("/checkout")
    }

    return (
        <Grid container sx={{ mb: "30px", alignItems: "center", height: "300px" }}>
            <Grid item sm={12} md={12} lg={12} xl={5} xxl={5} sx={{ borderRadius: '5px', mx: "auto" }}>
                <Typography sx={{ fontFamily: "Inter" }} align="center" variant="h4">You are logged in</Typography>
                <Button
                    sx={{
                        backgroundColor: "#000000",
                        ":hover": { backgroundColor: "#000000", },
                        mt: 5
                    }}
                    fullWidth
                    variant="contained"
                    size="large"
                    onClick={onBtnLogOut}>
                    Log out
                </Button>
                <Button
                
                sx={{
                    backgroundColor: "#000000",
                    ":hover": { backgroundColor: "#000000", },
                    mt: 5
                }}
                fullWidth
                variant="contained"
                size="large"
                onClick={payment}>
                continue payment
                </Button>
            </Grid>
        </Grid>
    )
}

export default LogOut