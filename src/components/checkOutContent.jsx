import { Box, Button, FormControl, FormHelperText, Grid, InputLabel, MenuItem, Select, TextField, Typography } from "@mui/material"
import { useEffect, useRef } from "react"
import { useDispatch, useSelector } from "react-redux"
import { callapiCountry, changeInputForm, changeSelectCity, changeSelectCountry, getUserInfo } from "../actions/product.action"
import { useForm } from "react-hook-form"
import { Toast } from "primereact/toast"

const CheckOutContent = () => {
    const toast = useRef(null);
    const dispatch = useDispatch()
    const { listOrder, total } = useSelector((redux) => redux.CartReducer)
    const { selectCountry, selectCity, customer } = useSelector((redux) => redux.CheckoutReducer)
    const { register, handleSubmit, formState: { errors } } = useForm({
        mode: "onChange",
        reValidateMode: "onChange",
        values: {
            "fullName": customer.fullName
        }
    })
    console.log(customer)
    useEffect(() => {
        const token = JSON.parse(localStorage.getItem("token"))
        dispatch(callapiCountry());
        dispatch(getUserInfo(token));
    }, [])

    const onChangeInputForm = (event, id) => {
        dispatch(changeInputForm(event.target.value, id))
    }

    const onChangSelectCountry = (event) => {
        dispatch(changeSelectCountry(event.target.value))

    };
    const onChangSelectCIty = (event) => {
        dispatch(changeSelectCity(event.target.value))
    };

    function handleSubmitUser(data) {
        // event.preventDefault();
        console.log(data)
    };
    return (
        <div className="container mb-[3rem]">
            <Toast ref={toast} />
            <Box component="form" noValidate onSubmit={handleSubmit(handleSubmitUser)}>
                <Grid container justifyContent="space-between">
                    <Grid item sm={12} md={12} lg={12} xl={7} xxl={7} sx={{ width: "100%", mt: "2rem" }} >
                        <Typography variant="h4">Infomatinon</Typography>

                        <TextField sx={{ mt: '40px' }} fullWidth variant="outlined" label="Name" type="text" value={customer.fullName || ""}
                            {...register("fullName", {
                                onChange: e => { onChangeInputForm(e, "fullName") },
                                required: "Name is required.",
                            })}
                            error={errors.fullName ? true : false}
                            helperText={errors.fullName?.message}

                        ></TextField>
                        <TextField sx={{ mt: '40px' }} fullWidth variant="outlined" label="Phone" value={customer.phone || ""}
                            {...register("phone", {
                                onChange: (event) => onChangeInputForm(event, "phone"),
                                required: "Phone is required.",
                                pattern: {
                                    value: /\d+/,
                                    message: "Phone is number only."
                                },
                                minLength: {
                                    value: 10,
                                    message: "Phone number must have 10 characters"
                                }
                            })}
                            error={errors.phone ? true : false}
                            helperText={errors.phone?.message}

                        />
                        <TextField sx={{ mt: '40px' }} fullWidth variant="outlined" label="Email" value={customer.email || ""}
                            {...register("email", {
                                onChange: (event) => onChangeInputForm(event, "email"),
                                required: "Email is required",
                                pattern: {
                                    value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                                    message: "Invalid email address."
                                }
                            })}
                            error={errors.email ? true : false}
                            helperText={errors.email?.message}
                        />
                        <TextField sx={{ mt: '40px' }} fullWidth variant="outlined" label="Address" value={customer.address || ""}
                            {...register("address", {
                                onChange: (event) => onChangeInputForm(event, "address"),
                                required: "Address is required"
                            })}
                            error={errors.address ? true : false}
                            helperText={errors.address?.message}

                        />
                        <FormControl sx={{ mt: '40px' }} error={errors.country ? true : false} fullWidth>
                            <InputLabel>Country</InputLabel>
                            <Select variant="outlined"
                                label="Country"
                                {...register("country", {
                                    onChange: (event) => onChangSelectCountry(event),
                                    pattern: {
                                        value: /[^0]/,
                                        message: "country is required"
                                    }
                                })}
                                value={customer.country || "0"}
                            >
                                <MenuItem value="0">None</MenuItem>
                                {
                                    selectCountry.map((item, index) => {
                                        return <MenuItem key={index} value={item.country}>{item.country}</MenuItem>
                                    })
                                }
                            </Select>
                            <FormHelperText>{errors.country?.message}</FormHelperText>
                        </FormControl>
                        <FormControl fullWidth error={errors.city ? true : false} sx={{ mt: '40px' }}>
                            <InputLabel>City</InputLabel>
                            <Select
                                variant="outlined" label="City"
                                {...register("city", {
                                    onChange: (event) => onChangSelectCIty(event),
                                    pattern: {
                                        value: /[^0]/,
                                        message: "City is required"
                                    }
                                })}
                                value={customer.city || "0"}

                            >
                                <MenuItem value="0">None</MenuItem>
                                {
                                    selectCity.map((item, index) => {
                                        return <MenuItem key={index} value={item}>{item}</MenuItem>
                                    })
                                }
                            </Select>
                            <FormHelperText>{errors.city?.message}</FormHelperText>
                        </FormControl>
                    </Grid>

                    <Grid item sm={12} md={12} lg={12} xl={4} xxl={4} sx={{ width: "100%", height: "100%", mt: "2rem" }}>
                        <Typography variant="h4" >Your Order</Typography>
                        <Box sx={{ bgcolor: "#f0f0f7", border: "solid 1px #c4c4c4", mt: '40px', borderRadius: "5px", p: 3 }}>
                            <Grid container justifyContent="space-between">
                                <Grid item sm={6} md={6} lg={6} xl={6} xxl={6}>
                                    <Typography variant="body1" sx={{ fontFamily: "Inter" }}>Product</Typography>
                                </Grid>
                                <Grid item sm={5} md={5} lg={5} xl={5} xxl={5} >
                                    <Typography align="center" variant="body1" sx={{ fontFamily: "Inter" }}>SubTotal</Typography>
                                </Grid>
                            </Grid>
                            <hr />
                            {
                                listOrder.map((items, index) => {
                                    return <Grid key={index} container justifyContent="space-between" sx={{ my: 4 }}>
                                        <Grid item sm={6} md={6} lg={6} xl={6} xxl={6}>
                                            <Typography variant="subtitle1" >  {items.name} X {items.quantity}</Typography>
                                        </Grid>
                                        <Grid item sm={5} md={5} lg={5} xl={5} xxl={5} >
                                            <Typography align="center" variant="subtitle1">$ {items.promotionPrice * items.quantity}</Typography>
                                        </Grid>
                                    </Grid>
                                })
                            }
                            <hr />
                            <Grid container justifyContent="space-between" alignItems="center">
                                <Grid item sm={6} md={6} lg={6} xl={6} xxl={6}>
                                    <Typography variant="subtitle1" sx={{ fontFamily: "Inter" }}>Total</Typography>
                                </Grid>
                                <Grid item sm={5} md={5} lg={5} xl={5} xxl={5} >
                                    <Typography align="center" variant="h5" sx={{ fontFamily: "Inter" }}>$ {total}</Typography>
                                </Grid>
                            </Grid>
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                size="large"
                                sx={{
                                    mt: 3,
                                    backgroundColor: "#000000",
                                    ":hover": { backgroundColor: "#000000", }
                                }}
                            >
                                PAY NOW

                            </Button>
                        </Box>

                    </Grid>
                </Grid>
            </Box>
        </div>
    )
}

export default CheckOutContent