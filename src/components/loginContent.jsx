import { Person2Outlined, VisibilityOffOutlined, VisibilityOutlined } from "@mui/icons-material"
import { Box, Button, FormControl, FormHelperText, Grid, IconButton, InputAdornment, InputLabel, OutlinedInput, Typography } from "@mui/material"
import React, { useEffect, useRef, useState } from "react";
import { useForm } from "react-hook-form";
import { Link, useNavigate } from "react-router-dom";
import { Toast } from 'primereact/toast';
import LogOut from "./logOut";
import { useDispatch, useSelector } from "react-redux";
import { CallApiLogin } from "../actions/product.action";


const LoginContent = () => {
    const toast = useRef(null);
    const dispatch = useDispatch()
    const navigate = useNavigate()
    const [token, setToken] = useState(false)

    const { register, handleSubmit, reset, formState: { errors } } = useForm()
    const [showPassword, setShowPassword] = React.useState(false);
    const handleClickShowPassword = () => setShowPassword((show) => !show);

    const getToken = localStorage.getItem("token")
    useEffect(() => {
        if (getToken) {
            setToken(true)
        }
    }, [token])

    const handleSubmitLogin = (data) => {
        const toasts = (severity, mes) => toast.current.show({ severity: severity, detail: mes, life: 2000 });
        dispatch(CallApiLogin(data, toasts, setToken, reset));

    }
    return (
        <div className="container mt-[1rem] mb-[3rem]">
            <Toast ref={toast} />
            {
                token ?
                    <LogOut setToken={() => setToken(false)} />
                    : <Grid container sx={{ mb: "25px" }}>
                        <Grid item sm={12} md={12} lg={12} xl={5} xxl={5} sx={{ borderRadius: '5px', mx: "auto", border: "solid 1.5px" }} >
                            <Box sx={{ p: 5 }} component="form" onSubmit={handleSubmit(handleSubmitLogin)}>
                                <Typography variant="h3" className="">Login</Typography>
                                <Typography sx={{ mt: 1 }} variant="h5" className="">to coutinue</Typography>
                                <FormControl error={errors.username ? true : false} sx={{ mt: '60px', width: '100%' }} variant="outlined">
                                    <InputLabel >Username</InputLabel>
                                    <OutlinedInput
                                        sx={{ borderRadius: "8px" }}
                                        label="Username"
                                        endAdornment={
                                            <InputAdornment position="end">
                                                <IconButton>
                                                    <Person2Outlined />
                                                </IconButton>
                                            </InputAdornment>
                                        }
                                        {...register("username", { required: "Username is required." })}

                                    />
                                    <FormHelperText>{errors.username?.message}</FormHelperText>
                                </FormControl>

                                <FormControl error={errors.password ? true : false} sx={{ mt: '40px', width: '100%' }} variant="outlined">
                                    <InputLabel >Password</InputLabel>
                                    <OutlinedInput
                                        label="password"
                                        sx={{ borderRadius: "8px" }}
                                        type={showPassword ? 'text' : 'password'}
                                        endAdornment={
                                            <InputAdornment position="end">
                                                <IconButton
                                                    aria-label="toggle password visibility"
                                                    onClick={handleClickShowPassword}
                                                >
                                                    {showPassword ? <VisibilityOffOutlined /> : <VisibilityOutlined />}
                                                </IconButton>
                                            </InputAdornment>
                                        }
                                        {...register("password", { required: "Password is required." })}
                                    />
                                    <FormHelperText>{errors.password?.message}</FormHelperText>
                                </FormControl>
                                <FormControl sx={{ mt: 7, width: "100%" }}>
                                    <Button
                                        type="submit"
                                        sx={{
                                            backgroundColor: "#000000",
                                            ":hover": { backgroundColor: "#000000", }
                                        }}
                                        variant="contained"
                                        size="large">
                                        Login
                                    </Button>
                                </FormControl>
                                <Typography sx={{ mt: 7 }} align="center">
                                    New User? <Link to="/signup" > <b>Register</b></Link>
                                </Typography>

                            </Box>
                        </Grid>
                    </Grid>
            }
        </div>
    )
}

export default LoginContent