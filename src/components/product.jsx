import { useDispatch, useSelector } from "react-redux"
import { clearData, getProductList, pageChangePagination } from "../actions/product.action"
import { useEffect } from "react"
import CardPoduct from "./cardProducts"
import ProductFilter from "./productFilter"
import { CircularProgress, Grid, Pagination } from "@mui/material"

const Product = () => {
    const dispatch = useDispatch()

    const { productList, min, max, brank, limit, noPage, currentPage, pending } = useSelector((redux) => redux.FilterReducer)

    useEffect(() => {
        
    dispatch(getProductList(currentPage, limit, brank, min, max))
    }, [currentPage]);
   
    const onChangePagination = (event, value) => {
        dispatch(pageChangePagination(value))

    }
    return (
        <div className="grid grid-cols-5 max-w-[1688px] mx-auto container my-4 px-2">
            <ProductFilter />
            {pending ?
                <div className="mx-auto">
                    <CircularProgress />
                </div>
                : <div className="col-span-4">
                    <div className="grid grid-cols-4 gap-[30px]">
                        {
                           productList && productList.length > 0 && productList.map((item, index) => {
                                return <CardPoduct key={index} data={item} />
                            })
                        }
                    </div>

                    <div className="flex justify-center my-4">
                        {productList && <Pagination onChange={onChangePagination} page={currentPage} count={noPage} />}
                    </div>
                </div>
            }
        </div>
    )
}
export default Product