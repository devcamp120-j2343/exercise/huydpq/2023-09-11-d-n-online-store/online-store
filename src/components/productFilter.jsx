import { useDispatch, useSelector } from "react-redux"
import { changeInputFilter, getProductList, onChangeCheckBox, onClickFilter } from "../actions/product.action"
import { Col, FormGroup, Label } from "reactstrap"
import { FormControlLabel, Radio, RadioGroup, TextField } from "@mui/material"

const ProductFilter = () => {
    const dispatch = useDispatch()
    const { min, max, limit, currentPage, brank } = useSelector((redux) => redux.FilterReducer)

    const changeCheckBox = (event, id) => {
        const value = event.target.checked ? event.target.value : event.target.checked;

        dispatch(onChangeCheckBox(value, id))
    }

    const onChangeInputFilter = (event, id) => {
        dispatch(changeInputFilter(event.target.value, id))
    }

    const onBtnFilterClick = () => {
        dispatch(onClickFilter())
        dispatch(getProductList(currentPage, limit, brank, min, max))
    }
    return (
        <div>
            <div className="mb-16">
                <label className="text-[#1d1d1f] font-Inter text-base mb-3">Brands</label>
                <FormGroup row>
                    <RadioGroup>
                        <Col sm={12}>
                            <FormControlLabel control={<Radio value="6501d3454e89340f69f8b2a6" onChange={(event) => changeCheckBox(event, "brank")} color="default" />} label="Iphone" />
                        </Col>
                        <Col sm={12}>
                            <FormControlLabel control={<Radio value="6501d35a4e89340f69f8b2a9" onChange={(event) => changeCheckBox(event, "brank")} color="default" />} label="Samsung" />
                        </Col>

                    </RadioGroup>
                </FormGroup>
            </div>
            <div className="mb-16">
                <label className="text-[#1d1d1f] font-Inter text-base mb-8">Price</label>
                <FormGroup className="mb-4" row>
                    <Label sm={2}>Min</Label>
                    <Col sm={10}>
                        <TextField onChange={(event) => onChangeInputFilter(event, "min")} size="small" sx={{ width: "50%" }} />

                    </Col>

                </FormGroup>
                <FormGroup row>
                    <Label sm={2}>Max</Label>
                    <Col sm={10}>
                        <TextField onChange={(event) => onChangeInputFilter(event, "max")} size="small" sx={{ width: "50%" }} />
                    </Col>

                </FormGroup>
            </div>
            <div className="mt-[6rem]">
                <button onClick={onBtnFilterClick} className=" bg-black font-medium text-white p-2 w-32 rounded ">Filter</button>
            </div>
        </div>
    )
}

export default ProductFilter