
const CardPoduct = ({ data }) => {  
    const url = (`/product/${data._id}`)
   
    return (
        <div className="flex flex-col mb-7 text-center">
            <img src={data.imageUrl} alt="a" className="" />
            <p className=" mx-2 mt-3 p-2 text-black text-xl not-italic font-semibold">{data.name}</p>
            <p className="text-[#637381] line-through text-base font-normal not-italic">${data.buyPrice}</p>
            <p className="text-[#1d1d1f] text-lg font-semibold">${data.promotionPrice}</p>
            <a href={url} className="text-[#06c] text-base">Learn more</a>
        </div>
    )
}

export default CardPoduct